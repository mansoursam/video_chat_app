import { IoAdapter } from "@nestjs/platform-socket.io";
import {
    MessageBody,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
  } from "@nestjs/websockets";
  import { Server } from "socket.io";
   
  @WebSocketGateway()
  export class ChatGateway {
    @WebSocketServer()
    server: Server;
   
    @SubscribeMessage("call")
    handleCall(@MessageBody() data: any) {
      this.server.on("connection", (socket)=>{
        socket.emit("me", socket.id);

        socket.on("disconnect", ()=>{
            socket.broadcast.emit("cancelled");
        });

        socket.on("callUser",({userToCall,signal, from, name})=>{
            socket.to(userToCall).emit("callUser", {signal, from ,name})
        });
        socket.on("answerCall", (data)=>{
            socket.to(data.to).emit("callAccepted", data.signal);
        })
      });
    }
  }
import * as Express from 'express';
import { Get, Response, Controller, UseGuards, Post, Body } from '@nestjs/common';
import { PusherService } from './pusher.service';
import { AppService } from './app.service';

@Controller()
export class AppController {
    constructor(private readonly pusherService: PusherService) { }
    // constructor(private readonly appService: AppService) { }

    @Post()
    async messages(
        @Body("username") username: string,
        @Body("message") message: string,
    ) {
        await this.pusherService.trigger("chat", "message", {
            username,
            message
        });
        return [];
    }
    // @Get()
    // getHello():string {
    //     return this.appService.getHello();
    // }
    
}
